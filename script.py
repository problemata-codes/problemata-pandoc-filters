import pypandoc

examples = [
    {
        "filter": "examples/every_str.py",
        "filename": "examples/every_str_sample.md"
    },
    {
        "filter": "examples/o_title.py",
        "filename": "examples/o_title_sample.md"
    },
    {
        "filter": "examples/o_media.py",
        "filename": "examples/o_media_sample.md"
    },
    {
        "filter": "examples/o_hyperlink.py",
        "filename": "examples/o_hyperlink_sample.md"
    },
    {
        "filter": "examples/o_noticemedia.py",
        "filename": "examples/o_noticemedia_sample.md"
    }
]

# choix du couple filter/filename
index = 4

filters = [examples[index]["filter"]]
filename = examples[index]["filename"]

# choix si l'output se fait vers un fichier ou dans le terminal
outputfile = True

if outputfile:
    output = pypandoc.convert_file(filename,
            to='html5',
            format='md',
            filters=filters,
            outputfile="examples/test.html")
else:
    output = pypandoc.convert_file(filename,
            to='html5',
            format='md',
            filters=filters)
    print(output)