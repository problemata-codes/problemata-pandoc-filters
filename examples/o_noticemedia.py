#!/usr/bin/env python

import requests
import simplejson as json
import re

from pandocfilters import walk, toJSONFilter, Str, Para, Image, Div, Plain, Link



# api_url = 'https://vincent-maillard.fr/projets/problemata/omeka_sample/api/items'
api_url = 'http://problemata.huma-num.fr/base/api/items'

def getFromOmeka(url):
    code = requests.get(url)
    plain = code.text
    return plain

def parseJsonItem(json_obj):
    py_obj = json.loads(json_obj)
    return py_obj


site = "http://problemata.huma-num.fr/fr/ressources/"

pattern = re.compile('\[\[(.*)\]\]$')

# cf. https://github.com/jgm/pandocfilters/blob/master/examples/metavars.py
def createNoticeMedia(key, value, format, meta):
    if key == 'Str':
        m = pattern.match(value)
        if m:
            item_id = m.group(1)
            item_url = "{}/{}".format(api_url,item_id)
            o_item = parseJsonItem(getFromOmeka(item_url))

            # création de la div contenant l'img
            media = o_item.get('pb:media', "")
            img_dest = media[0].get('o:thumbnail_urls', "").get('large', "")
            img = Plain([Image(['', [], []], [], [img_dest, ""])])
            div_img = Div(['', [], []], [img])

            # création du lien vers la ressource django
            django_url = "{}/{}".format(site,item_id)
            attr = [
                ["target", "_blank"]
            ]
            django_link = Link(['', [], attr], [Str("Base")], [django_url, "title"])
            django_plain_link = Plain([django_link])

            # création du lien vers la ressource ds l'api Omeka
            omeka_url = o_item.get('@id', "")
            attr = [
                ["target", "_blank"]
            ]
            omeka_link = Link(['', [], attr], [Str("Api")], [omeka_url, "title"])
            omeka_plain_link = Plain([omeka_link])

            # merge des trois éléments
            div_notice = Div(['', [], []], [div_img, django_plain_link, omeka_plain_link])
            return div_notice

def getPara(key, value, format, meta):
    if key == 'Para':
        # exception pr un seul élément dans le Para
        if (len(value) == 1):
            new_value = walk(value, createNoticeMedia, format, meta)
            # si le contenu a été modifié suite à walk()
            if new_value != value:
                return new_value

if __name__ == "__main__":
  toJSONFilter(getPara)