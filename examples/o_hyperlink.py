#!/usr/bin/env python

import requests
import simplejson as json
import re

from pandocfilters import walk, toJSONFilter, Str, Para, Image, Link



# api_url = 'https://vincent-maillard.fr/projets/problemata/omeka_sample/api/items'
api_url = 'http://problemata.huma-num.fr/base/api/items'

def getFromOmeka(url):
    code = requests.get(url)
    plain = code.text
    return plain

def parseJsonItem(json_obj):
    py_obj = json.loads(json_obj)
    return py_obj



pattern = re.compile('\[\[(.*)\]\]$')

# cf. https://github.com/jgm/pandocfilters/blob/master/examples/metavars.py
def getMedia(key, value, format, meta):
    if key == 'Str':
        m = pattern.match(value)
        if m:
            item_id = m.group(1)
            item_url = "{}/{}".format(api_url,item_id)
            o_item = parseJsonItem(getFromOmeka(item_url))
            url = o_item.get('@id', "")
            
            attr = [
                ["target", "_blank"]
            ]
            return Link(['', [], attr], [Str("text")], [url, "title"])

def getPara(key, value, format, meta):
    if key == 'Para':
        return Para(walk(value, getMedia, format, meta))
        

if __name__ == "__main__":
  toJSONFilter(getPara)