#!/usr/bin/env python

import requests
import simplejson as json
import re

from pandocfilters import toJSONFilter, Str



# api_url = 'https://vincent-maillard.fr/projets/problemata/omeka_sample/api/items'
api_url = 'http://problemata.huma-num.fr/base/api/items'

def getFromOmeka(url):
    code = requests.get(url)
    plain = code.text
    return plain

def parseJsonItem(json_obj):
    py_obj = json.loads(json_obj)
    return py_obj



pattern = re.compile('\[\[(.*)\]\]$')

# cf. https://github.com/jgm/pandocfilters/blob/master/examples/metavars.py
def getTitle(key, value, format, meta):
    if key == 'Str':
        m = pattern.match(value)
        if m:
            item_id = m.group(1)
            item_url = "{}/{}".format(api_url,item_id)
            o_item = parseJsonItem(getFromOmeka(item_url))
            title = o_item.get('o:title', "[UNTITLED]")
            return Str(title)

if __name__ == "__main__":
  toJSONFilter(getTitle)