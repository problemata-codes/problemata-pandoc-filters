#!/usr/bin/env python

import requests
import simplejson as json

from pandocfilters import toJSONFilter, Str



api_url = 'https://vincent-maillard.fr/projets/problemata/omeka_sample/api/items'
# api_url = 'http://problemata.huma-num.fr/base/api/items'

def getFromOmeka(url):
    code = requests.get(url)
    plain = code.text
    return plain

def parseJsonItem(json_obj):
    py_obj = json.loads(json_obj)
    return py_obj



# cf. https://github.com/jgm/pandocfilters/blob/master/examples/caps.py
def caps(key, value, format, meta):
  if key == 'Str':
    article_tpl = "resource_template_id=5"
    item_url = "{}?{}".format(api_url,article_tpl)
    o_items = parseJsonItem(getFromOmeka(item_url))
    test = o_items[1]['o:title']
    return Str(test)

if __name__ == "__main__":
  toJSONFilter(caps)
