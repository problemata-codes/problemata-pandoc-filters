# Problemata Pandoc filters



## Installation

- `source env/bin/activate` (testé avec Python 3.9)
- `pip install -r requirements.txt` (suppose Pandoc installé en global)



## Utilisation

Il y a deux utilisations possibles : par `pypandoc` (plus simple et sûrement compatible avec Django) ou par `pandoc`

---

Par `pypandoc` :
- `source env/bin/activate` (testé avec Python 3.9)
- ouvrir `script.py` et choisir un exemple en modifiant "index" 
- `python script.py`

L'output se fait soit dans le terminal soit dans "example/test.html".

---

Par `pandoc` : 
- `source env/bin/activate` (testé avec Python 3.9)
- `pandoc --filter examples/{filter} examples/{input} -o examples/{output}`

Exemple : `pandoc --filter examples/o_title.py examples/o_title_sample.md -o examples/test.html`

---


## Filtres

**``every_str.py``**

Chaque `Str` est remplacé par la titre d'un des items de la base Omeka.


**``o_title.py``**

Chaque `[[o_id]]` est remplacé par le titre (`o:title`) de l'o_id. Qu'il soit compris dans une ligne ou une ligne seule.


**``o_media.py``**

Chaque `[[o_id]]` est remplacé par la première image de l'o_id. Qu'il soit compris dans une ligne ou une ligne seule.


**``o_hyperlink.py``**

Chaque `[[o_id]]` est remplacé par un lien vers l'item dans l'API Omeka. Qu'il soit compris dans une ligne ou une ligne seule.


**``o_noticemedia.py``**

Chaque `[[o_id]]`, étant le seul élément sur une ligne, est remplacé par une `div[ div[img], a, a]` où : l'`img` est le premier média de l'item Omeka, le premier `a` dirige vers l'item sur Django et le second `a` dirige vers l'item dans l'API Omeka.



## Exemples

- %{fields} : https://github.com/sergiocorreia/panflute/blob/master/examples/pandocfilters/metavars.py


## Constructeurs

### (Block) Div

```
content = Plain([Str("test")]) (Liste de Blocks)
identifier = 'mon_id'
classes = ["ma_class"]
attributes = [
    ["data-test", "test"]
]
return Div([identifier, classes, attributes], [content])
```

### (Block) Para

```
content = Str("test") (Liste de Inlines)
return Para([content])
```

### (Block) Plain

```
content = Str("test") (Liste de Inlines)
return Plain([content])
```

### (Inline) Link

```
content = Str("mon_texte") (Liste de Inlines)
target = "mon_url"
title = "mon_titre"
identifier = 'mon_id'
classes = ["ma_class"]
attributes = [
    ["target", "_blank"]
]
return Link([identifier, classes, attributes], [content], [target, title])
```

### (Inline) Image

```
caption = Str("mon_texte") (Liste de Inlines)
src = "mon_url"
title = "mon_titre"
identifier = "mon_id"
classes = ["ma_class"]
attributes = [
    ["data-test", "test"]
]
return Image([identifier, classes, attributes], [caption], [src, title])
```

### (Inline) Str

```
text = "mon_texte"
return Str(text)
```


## Ressources

- doc Pandoc : https://pandoc.org/filters.html
- pandocfilters package (python) : https://github.com/jgm/pandocfilters/
- panflute package (python) : https://github.com/sergiocorreia/panflute
- https://pandoc.org/lua-filters#module-pandoc
- appel à des fonctions pour générer un objet dans la génération d'un objet :
    - https://github.com/jgm/pandocfilters/blob/master/examples/deflists.py
    - https://github.com/jgm/pandocfilters/blob/master/examples/theorem.py
- fetch (source, base_url) : Fetches the given source from a URL or local file. Returns two values: the contents of the file and the MIME type (or an empty string).
    - pandoc.mediabag.fetch(diagram_url, ".")
- https://metacpan.org/pod/Pandoc::Elements#Image